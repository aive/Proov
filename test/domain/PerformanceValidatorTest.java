package domain;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PerformanceValidatorTest {

    private PerformanceCreateCommand performanceCreateCommand = mock(PerformanceCreateCommand.class);
    private PerformanceValidator performanceValidator = new PerformanceValidator();

    @Test
    public void validateIfFieldIsEmpty() throws Exception {
        when(performanceCreateCommand.getEvent()).thenReturn("RACE100M");
        when(performanceCreateCommand.getResult()).thenReturn("");

        Map<String, String> result = performanceValidator.validate(performanceCreateCommand);

        assertEquals("All fields must be filled", result.get("emptyError"));
    }


    @Test
    public void validateDateIfResultIsCorrect() throws Exception {
        when(performanceCreateCommand.getEvent()).thenReturn("LONGJUMP");
        when(performanceCreateCommand.getResult()).thenReturn("123");

        Map<String, String> result = performanceValidator.validate(performanceCreateCommand);

        assertEquals(0, result.size());
    }

    @Test
    public void validateDateIfDateIsIncorrect() throws Exception {
        when(performanceCreateCommand.getEvent()).thenReturn("RACE100M");
        when(performanceCreateCommand.getResult()).thenReturn("1,23");

        Map<String, String> result = performanceValidator.validate(performanceCreateCommand);

        assertEquals("The result of the race has to be in format (mm:)ss.SS(S)", result.get("timeError"));
    }

}
