package domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalcServiceTest {
    CalcService calcService = new CalcService();
    Performance performance = mock(Performance.class);

    @Before
    public void setUp() {
        CalcService calcService = new CalcService();
        Performance performance = mock(Performance.class);
  }

    @Test
    public void calculatePointsInCaseOfRace() throws Exception {
        when(performance.getEvent()).thenReturn("RACE100M");
        when(performance.getResult()).thenReturn(9.58);
        assertEquals(1202, calcService.calculatePoints(performance));
    }

    @Test
    public void calculatePointsInCaseOfJump() throws Exception {
        when(performance.getEvent()).thenReturn("LONGJUMP");
        when(performance.getResult()).thenReturn(8.95);
        assertEquals(1312, calcService.calculatePoints(performance));
    }

    @Test
    public void calculatePointsInCaseOfThrow() throws Exception {
        when(performance.getEvent()).thenReturn("DISCUSTHROW");
        when(performance.getResult()).thenReturn(74.08);
        assertEquals(1383, calcService.calculatePoints(performance));
    }
}