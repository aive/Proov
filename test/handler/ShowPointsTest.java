package handler;

import domain.CalcService;
import domain.Performance;
import org.junit.Before;
import org.junit.Test;
import spark.ModelAndView;
import spark.Request;
import util.NumberUtil;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ShowPointsTest {

    ShowPoints showPoints = new ShowPoints();
    NumberUtil numberUtil = mock(NumberUtil.class);
    CalcService calcService = mock(CalcService.class);
    Performance performance = mock (Performance.class);

    @Before
    public void setUp() {
        showPoints.numberUtil = numberUtil;
        showPoints.calcService = calcService;
        when(numberUtil.parse(anyString())).thenCallRealMethod();
        when(numberUtil.format(anyObject())).thenCallRealMethod();

    }
    @Test
    public void handle() throws Exception {

        Request request = mock(Request.class);
        when(request.params("event")).thenReturn("LONGJUMP");
        when(request.params("result")).thenReturn("6.6");
        when(calcService.calculatePoints(performance)).thenReturn(720);

        ModelAndView result = showPoints.handle(request, null);
        assertEquals("calculated-points.vm", result.getViewName());
        Map<String, Object> model = (Map<String, Object>) result.getModel();
        Object event = model.get("event");
        Object sportResult = model.get("result");
        assertEquals(3, model.size());
        assertEquals("LONGJUMP", event.toString());
        assertEquals("6.6", sportResult.toString());
        assertSame(event, model.get("event"));





    }



}