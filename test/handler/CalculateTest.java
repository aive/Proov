package handler;
        import java.util.Map;

        import domain.PerformanceCreateCommand;
        import org.junit.Test;
        import spark.ModelAndView;
        import spark.Request;
        import spark.Response;

        import static org.junit.Assert.assertEquals;
        import static org.mockito.Mockito.*;

public class CalculateTest {

    private Calculate calculate = new Calculate();

    @Test
    public void handleIfErrorsOccur() throws Exception {
        Request request = mock(Request.class);
        Response response = mock(Response.class);
        when(request.queryParams("event")).thenReturn("RACE100M");
        when(request.queryParams("result")).thenReturn("");

        ModelAndView result = calculate.handle(request, response);

        Map<String, Object> model = (Map<String, Object>) result.getModel();
        Map<String, String> errors = (Map<String, String>) model.get("errors");
        PerformanceCreateCommand performance = (PerformanceCreateCommand) model.get("performance");

        assertEquals("calculator.vm", result.getViewName());
        assertEquals("All fields must be filled", errors.get("emptyError"));
        assertEquals("RACE100M", performance.getEvent());
    }

    @Test
    public void handleIfNoErrors() throws Exception {
        Request request = mock(Request.class);
        Response response = mock(Response.class);
        when(request.queryParams("event")).thenReturn("LONGJUMP");
        when(request.queryParams("result")).thenReturn("123");

        ModelAndView result = calculate.handle(request, response);

        assertEquals("calculator.vm", result.getViewName());
        verify(response).redirect("/points/LONGJUMP/123");
    }
}