package handler;

import domain.CalcService;
import spark.*;

import javax.inject.Inject;
import java.util.*;

public class CalcForm implements TemplateViewRoute {

    @Inject
    CalcService calcService;

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        List<CalcService.Event> events = calcService.getEvents();
        Map<String, Object> model = new HashMap<>();
        model.put("events", events);
        return new ModelAndView(model, "calculator.vm");
    }
}
