package handler;

import domain.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.NumberUtil;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

public class ShowPoints implements TemplateViewRoute {
    @Inject
    NumberUtil numberUtil;
    @Inject
    CalcService calcService;

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String event = request.params("event");
        String stringResult = request.params("result");

        double result = numberUtil.parse(stringResult);
        Performance performance = new Performance(event, result);
        int points = calcService.calculatePoints(performance);
        String formattedPoints = numberUtil.format(points);

        Map<String, Object> model = new HashMap<>();
        model.put("event", event);
        model.put("result", result);
        model.put("points", formattedPoints);
        return new ModelAndView(model, "calculated-points.vm");
    }
}
