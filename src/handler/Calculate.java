package handler;

import domain.*;
import spark.*;

import java.util.HashMap;
import java.util.Map;

public class Calculate implements TemplateViewRoute {

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String event = request.queryParams("event");
        String result = request.queryParams("result");

        PerformanceCreateCommand performanceCreateCommand = new PerformanceCreateCommand(event, result);
        PerformanceValidator performanceValidator = new PerformanceValidator();
        CalcService calcService = new CalcService();
        Map<String, String> errors = performanceValidator.validate(performanceCreateCommand);

        Map<String, Object> model = new HashMap<>();
        if (!errors.isEmpty()) {
            model.put("errors", errors);
            model.put("performance", performanceCreateCommand);
            model.put("events", calcService.getEvents());
            return new ModelAndView(model, "calculator.vm");
        }
        response.redirect("/points/" + event + "/" + result);

        return new ModelAndView(model, "calculator.vm");
    }
    }

