package util;

import javax.inject.Singleton;

@Singleton
public class NumberUtil {

    public Double parse(String time){

        double resultInSeconds;
        int colonPosition = time.indexOf(":");

        if(colonPosition>-1){
            String[] InputStringPartsByColon = time.split(":");
            double minutes = Double.parseDouble(InputStringPartsByColon[0]);
            double seconds = Double.parseDouble(InputStringPartsByColon[1]);

            resultInSeconds = minutes*60 + seconds;
        }
        else{
            resultInSeconds = Double.parseDouble(time);
        }
        return resultInSeconds;
    }

    public String format(Double number) {
        return Double.toString(number);
    }

    public String format (int number) { return Integer.toString(number); }
}