package util;
import com.google.inject.Guice;
import com.google.inject.Injector;

import handler.CalcForm;
import handler.Calculate;
import handler.ShowPoints;
import spark.template.velocity.VelocityTemplateEngine;

import static spark.Spark.*;

public class Server {
    public static void main(String[] args) {
        VelocityTemplateEngine templateEngine = new VelocityTemplateEngine();

        Injector injector = Guice.createInjector();
        get("/", injector.getInstance( CalcForm.class), templateEngine);
        post("/",injector.getInstance( Calculate.class), templateEngine);
        get("/points/:event/:result", injector.getInstance( ShowPoints.class), templateEngine);
    }
}




