package domain;

public class PerformanceCreateCommand {
    private String event;
    private String result;

    public PerformanceCreateCommand(String event, String result) {
        this.event = event;
        this.result = result;
    }
    public String getEvent() {
        return event;
    }
    public String getResult() {
        return result;
    }
}