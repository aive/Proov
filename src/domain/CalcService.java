package domain;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static domain.CalcService.Event.*;

@Singleton
public class CalcService {

    public enum Event {

        RACE100M(25.4347, 18.0, 1.81, MeasurementUnit.SECONDS),
        LONGJUMP(0.14354, 220.0, 1.4, MeasurementUnit.METERS),
        SHOTPUT(51.39, 1.5, 1.05, MeasurementUnit.METERS),
        HIGHJUMP(0.8465, 75.0, 1.42, MeasurementUnit.METERS),
        RACE400M(1.53775, 82.0, 1.81, MeasurementUnit.SECONDS),
        RACE110MHURDLES(5.74352, 28.5, 1.92, MeasurementUnit.SECONDS),
        DISCUSTHROW(12.91, 4.0, 1.1, MeasurementUnit.METERS),
        POLEVAULTJUMP(0.2797, 100.0, 1.35, MeasurementUnit.METERS),
        JAVELINTHROW(10.14, 7.0, 1.08, MeasurementUnit.METERS),
        RACE1500M(0.03768, 480.0, 1.85, MeasurementUnit.MINUTES_SECONDS);

        public double A, B, C;
        public MeasurementUnit unit;

        Event(double A, double B, double C, MeasurementUnit unit) {
            this.A = A;
            this.B = B;
            this.C = C;
            this.unit = unit;
        }
    }

    public int calculatePoints(Performance performance) {

        Event eventEnum = Event.valueOf(performance.getEvent());

        if (performance.getEvent().startsWith("RACE")) {
            return (int) (eventEnum.A * Math.pow((eventEnum.B - performance.getResult()), eventEnum.C));
        }
        else if (performance.getEvent().endsWith("JUMP")) {
            return (int) (eventEnum.A * Math.pow(performance.getResult() * 100 - eventEnum.B, eventEnum.C));
        }
        else {
            return (int) (eventEnum.A * Math.pow(performance.getResult() - eventEnum.B, eventEnum.C));
        }
    }

    public List<Event> getEvents() {
        List<Event> events = new ArrayList<>();
        events.addAll(Arrays.asList(RACE100M, LONGJUMP, SHOTPUT, HIGHJUMP, RACE400M, RACE110MHURDLES, DISCUSTHROW, POLEVAULTJUMP, JAVELINTHROW, RACE1500M));
        return events;
    }
}
