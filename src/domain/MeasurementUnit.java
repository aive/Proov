package domain;

public enum MeasurementUnit {
    SECONDS,
    METERS,
    MINUTES_SECONDS,
}
