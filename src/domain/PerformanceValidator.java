package domain;

import util.NumberUtil;

import javax.inject.Singleton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class PerformanceValidator {

    public Map<String, String> validate(PerformanceCreateCommand performance) {
        String event = performance.getEvent();
        String result = performance.getResult();
        List<String> requestParameters = Arrays.asList(event, result);

        Map<String, String> errorMessages = new HashMap<>();
        if (containsEmptyParameters(requestParameters)) {
            errorMessages.put("emptyError", "All fields must be filled");
            return errorMessages;
        }
        if ((performance.getEvent().startsWith("RACE")) && (!isTimeValid(result))) {
            errorMessages.put("timeError", "The result of the race has to be in format (mm:)ss.SS(S)");
        }
        if ((!performance.getEvent().startsWith("RACE")) && (!isDistanceValid(result))) {
            errorMessages.put("distanceError", "The result has to be in meters 0.00");

        }
        return errorMessages;
    }

    private boolean containsEmptyParameters(List<String> requestParameters) {
        for (String parameter : requestParameters) {
            if (null == parameter || "".equals(parameter)) {
                return true;
            }
        }
        return false;
    }

    private boolean isTimeValid(String result) {
        NumberUtil numberUtil = new NumberUtil();
        try {
            numberUtil.parse(result);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isDistanceValid(String result) {
        try {
            Double.parseDouble(result.replaceAll(",", ".").replaceAll(" ", ""));
            Double.parseDouble(result);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
