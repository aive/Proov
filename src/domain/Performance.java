package domain;

public class Performance {
    public String event;
    public Double result;

    public Performance(String event, Double result) {
        this.event = event;
        this.result = result;
    }

    public String getEvent() {
        return event;
    }

    public Double getResult() {
        return result;
    }

}
